import { Component, OnInit } from '@angular/core';
import { HackDataModel} from 'src/app/hackdatamodel';
import { SyncDataModelService} from 'src/sync-data-model.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Hello Reto';
  newMessage = '';

  constructor(private syncdatamodelservice: SyncDataModelService) {
  }

  onAdd(message) {
    this.model.syncdata.push(message);
    this.syncdatamodelservice.updateModel(this.model);
  }

  ngOnInit() { }

  get model() {
    return this.syncdatamodelservice.getModel();
  }

}
