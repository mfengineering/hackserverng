import { Injectable } from '@angular/core';
import { HackDataModel} from 'src/app/hackdatamodel';
import { WebSocketHandler} from './websockethandler';

@Injectable({
  providedIn: 'root'
})

export class SyncDataModelService {
  ws: WebSocketHandler;
  model: HackDataModel = {
    localinfos : '',
    syncdata : ['Message1', 'Message2']
  };

  constructor() {
    this.ws = new WebSocketHandler(this.model);
  }

  getModel(): HackDataModel {
    return this.model;
  }

  updateModel(model: any) {
    this.ws.updateModel(model);
  }
}

