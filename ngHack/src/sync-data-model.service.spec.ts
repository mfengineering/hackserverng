import { TestBed, inject } from '@angular/core/testing';

import { SyncDataModelService } from './sync-data-model.service';

describe('SyncDataModelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SyncDataModelService]
    });
  });

  it('should be created', inject([SyncDataModelService], (service: SyncDataModelService) => {
    expect(service).toBeTruthy();
  }));
});
