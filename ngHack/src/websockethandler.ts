import { HackDataModel} from 'src/app/hackdatamodel';
import { logger} from "./logger";

export class WebSocketHandler {
  private ws: any = false;

  constructor(private model: HackDataModel) {
    this.InitWebSockets();
    window.setInterval(() => WebSocketHandler.CheckWebsockets(this), 5000);
  }

  InitWebSockets() {
    const address = WebSocketHandler.getAddress(window.location.host);
    const that = this;

    logger.log('Connecting websocket to ' + window.location.host);

    this.CloseWebsockets();

    try {
      this.ws = new WebSocket(address);
    } catch (err) {
      logger.log('Could not connect websocket: ' + err.message);
    }

    // Handlers
    this.ws.onopen = function () {
      logger.log('Websocket open');
      that.ws.send(JSON.stringify({command: 'getdata'}));
    };

    this.ws.onerror = function (error) {
      logger.log('WebSocket Error ' + error);
    };

    this.ws.onclose = function () {
      logger.log('Websocket Closed');
    };

    // Log messages from the server
    this.ws.onmessage = function (e) {
      const newmodel = JSON.parse(e.data);
      if (typeof newmodel.syncdata === 'undefined') {
        newmodel.syncdata = [];
      }
      that.model.syncdata = newmodel.syncdata;
    };
  }

  private static getAddress(url: String): string {
    if (url.indexOf(':4200') > 0) {
      // development mode with 'ng serve'
      // in this case the server is usually also started locally on port 3000
      return 'ws://localhost:3000';
    }
    return 'ws://' + url;
  }

  private CloseWebsockets() {
    if (this.ws) {
      try {
        this.ws.close();
      } catch (err) {
        logger.log('Websocket already closed');
      }
    }
  }

  // Checks Websockets and reconnects if no connection is available
  private static CheckWebsockets(handler: WebSocketHandler) {
    try {
      if (handler.ws.readyState === 0 || handler.ws.readyState === 1) {
        // WS is in connecting or open state. Everything ok
        return 0;
      }
    } catch (err) {}

    // WS is not in connectiong or open state
    logger.log('Connection lost, restarting Websockets');
    handler.InitWebSockets();
  }

  public updateModel(model: any) {
    try {
      this.ws.send(JSON.stringify({command: 'setdata', model: model}));
    } catch (err) {
      logger.log('Could not send on websocket: ' + err.message);
    }
  }
}
